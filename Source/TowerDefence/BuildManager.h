// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BuildManager.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FBuiltTower, int32, cost);
UCLASS()
class TOWERDEFENCE_API ABuildManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuildManager();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class ATowerGhost* currentGhost;

	UFUNCTION()
	void DeductGold(int32 cost);

	UPROPERTY(EditAnywhere, BlueprintAssignable)
	FBuiltTower OnTowerBuilt;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
