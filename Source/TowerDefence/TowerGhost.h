// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TowerGhost.generated.h"

UCLASS()
class TOWERDEFENCE_API ATowerGhost : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATowerGhost();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UTowerData* TowerData;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UMaterial* InvalidMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UMaterial* ValidMaterial;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY(EditAnywhere)
	TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypes;
	TArray<class UStaticMeshComponent*> towerMeshComponents;
	class APlayerController* playerController;
	void ChangeGhostColor();
	void FollowCursor();
	void PlaceTower(class ATowerNode* node);
	bool isBuildable;
	class ABuildManager* buildManager;
};
