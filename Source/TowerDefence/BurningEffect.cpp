// Fill out your copyright notice in the Description page of Project Settings.

#include "BurningEffect.h"
#include "Enemy.h"
#include "HealthComponent.h"

ABurningEffect::ABurningEffect()
{

}

void ABurningEffect::ApplyEffect()
{
	if (AEnemy* enemy = Cast<AEnemy>(parentTarget))
	{
		enemy->HealthComponent->TakeDamage(int32(effectPower), caster);
	}
}

void ABurningEffect::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	BeginEffectOverTime();
}
