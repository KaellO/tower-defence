// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Enemy.h"
#include "TargetDetectionComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FTargetEnter, AActor*, target);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FTargetExit, AActor*, target);
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOWERDEFENCE_API UTargetDetectionComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTargetDetectionComponent();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UCapsuleComponent* DetectionRadius;
protected:
	// Called when the game starts
	virtual void BeginPlay() override;


	UFUNCTION()
	void OnCapsuleBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
		const FHitResult& SweepResult);

	UFUNCTION()
		void OnCapsuleEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex);

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
		void DetectEnemies();

	UFUNCTION(BlueprintImplementableEvent)
	void OnDetectionEnter();

	UFUNCTION(BlueprintImplementableEvent)
	void OnDetectionExit();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Target)
	TArray<AActor*> targets;

	UPROPERTY(EditAnywhere, BlueprintAssignable)
	FTargetEnter OnTargetEnter;

	UPROPERTY(EditAnywhere, BlueprintAssignable)
	FTargetExit OnTargetExit;

private:
	UPROPERTY(EditDefaultsOnly, Category = Target)
	TSubclassOf<AActor> targetClass;

	UPROPERTY(EditDefaultsOnly, Category = Target)
	TArray<TEnumAsByte<EnemyType>> enemyType;
};
