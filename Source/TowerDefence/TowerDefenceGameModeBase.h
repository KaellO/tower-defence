// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TowerDefenceGameModeBase.generated.h"

/**
 * 
 */

UCLASS()
class TOWERDEFENCE_API ATowerDefenceGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 currentWave;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float totalEnemies;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float deadEnemies;

	UFUNCTION(BlueprintPure, BlueprintCallable)
		float UpdateWaveProgress();

	UFUNCTION(BlueprintCallable)
		void SpawnNextWave();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool canSpawnWave;

	UFUNCTION(BlueprintImplementableEvent)
		void WinGame();

protected:
	virtual void BeginPlay() override;

private:
	UFUNCTION()
	void OnEnemySpawned(class AEnemy* enemy);

	UFUNCTION()
	void OnEnemyDeath();

	void WaveComplete();

	FTimerHandle NextWaveHandle;

	TArray<class AEnemySpawner*> enemySpawners;
};
