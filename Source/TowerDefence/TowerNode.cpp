// Fill out your copyright notice in the Description page of Project Settings.


#include "TowerNode.h"
#include "Components/StaticMeshComponent.h"
#include "Tower.h"

// Sets default values
ATowerNode::ATowerNode()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));
	SetRootComponent(BaseMesh);
}

void ATowerNode::PlaceTower(ATower* tower)
{
	currentTower = tower;
	tower->OnTowerSold.AddDynamic(this, &ATowerNode::ClearNode);
}

// Called when the game starts or when spawned
void ATowerNode::BeginPlay()
{
	Super::BeginPlay();
	
}

void ATowerNode::ClearNode()
{
	currentTower = NULL;
}

// Called every frame
void ATowerNode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

