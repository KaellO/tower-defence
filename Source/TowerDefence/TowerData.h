// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "TowerData.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENCE_API UTowerData : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString name;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 cost;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 upgradeCost;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString description;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<class ATower> towerClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float range;
};
