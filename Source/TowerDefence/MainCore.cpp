// Fill out your copyright notice in the Description page of Project Settings.


#include "MainCore.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Enemy.h"

// Sets default values
AMainCore::AMainCore()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	SetRootComponent(StaticMesh);

	Hitbox = CreateDefaultSubobject<UBoxComponent>("Hitbox");
	Hitbox->SetupAttachment(StaticMesh);
	Hitbox->OnComponentHit.AddDynamic(this, &AMainCore::OnComponentHit);
}

// Called when the game starts or when spawned
void AMainCore::BeginPlay()
{
	Super::BeginPlay();
	
}

void AMainCore::OnComponentHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		OnTrigger(enemy);
		OnCoreHit.Broadcast(10);
	}
}

// Called every frame
void AMainCore::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

