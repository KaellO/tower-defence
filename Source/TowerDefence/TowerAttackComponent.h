// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TowerAttackComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOWERDEFENCE_API UTowerAttackComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTowerAttackComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	class AProjectile* ShootProjectile(TSubclassOf<class AProjectile> projectile, AActor* target, class UArrowComponent* barrel, bool isHoming = true);

	UFUNCTION(BlueprintCallable)
	bool ShootRay(class UArrowComponent* barrel, FVector target);

	UFUNCTION(BlueprintCallable)
	class AStatusEffect* SpawnStatusEffect(TSubclassOf<class AStatusEffect> statusEffectClass, AActor* target);

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool canShoot;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float fireRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 damage;
private:

	FTimerHandle fireRateHandle;

	void setCanShootTrue();
};
