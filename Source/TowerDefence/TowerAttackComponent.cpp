// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerAttackComponent.h"
#include "Projectile.h"
#include "Enemy.h"
#include "Tower.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/ArrowComponent.h"
#include "DrawDebugHelpers.h"
#include "StatusEffect.h"

// Sets default values for this component's properties
UTowerAttackComponent::UTowerAttackComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	canShoot = true;
	// ...
}


// Called when the game starts
void UTowerAttackComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

AProjectile* UTowerAttackComponent::ShootProjectile(TSubclassOf<AProjectile> projectile, AActor* target, UArrowComponent* barrel, bool isHoming)
{
	if (!projectile->IsValidLowLevel() || !target->IsValidLowLevel() || !canShoot)
		return NULL;

	if (AEnemy* enemy = Cast<AEnemy>(target))
	{
		FRotator Rotation = barrel->GetComponentRotation();
		FActorSpawnParameters SpawnInfo;

		FVector Location = barrel->GetComponentLocation();

		AProjectile* spawnedProjectile = GetWorld()->SpawnActor<AProjectile>(projectile, Location, Rotation, SpawnInfo);
		if (spawnedProjectile->IsValidLowLevel())
		{
			spawnedProjectile->towerDamageRef = damage;
			spawnedProjectile->towerRef = this->GetOwner();
			if (isHoming)
			{
				UProjectileMovementComponent* projectileMovement;
				projectileMovement = spawnedProjectile->FindComponentByClass<UProjectileMovementComponent>();
				projectileMovement->HomingTargetComponent = enemy->GetRootComponent();
			}
		}


		canShoot = false;
		GetWorld()->GetTimerManager().SetTimer(fireRateHandle, this, &UTowerAttackComponent::setCanShootTrue, fireRate, false);

		return spawnedProjectile;
	}
	else
		return NULL;
}

bool UTowerAttackComponent::ShootRay(UArrowComponent* barrel, FVector target)
{
	if (!canShoot)
		return false;

	canShoot = false;
	FCollisionQueryParams CollisionParams;

	FHitResult OutHit;

	bool isHit = GetWorld()->LineTraceSingleByChannel(OutHit, barrel->GetComponentLocation(), target, ECC_WorldDynamic, CollisionParams);

	if (isHit)
	{
		DrawDebugLine(GetWorld(), barrel->GetComponentLocation(), target, FColor::Red, false, -1, 0, 30);

		GetWorld()->GetTimerManager().SetTimer(fireRateHandle, this, &UTowerAttackComponent::setCanShootTrue, fireRate, false);

		return true;
	}


	return false;
}

AStatusEffect* UTowerAttackComponent::SpawnStatusEffect(TSubclassOf<class AStatusEffect> StatusEffectClass, AActor* target)
{
	if (!StatusEffectClass->IsValidLowLevel() || !target->IsValidLowLevel())
		return nullptr;


	TArray<AActor*> attachedActors;
	target->GetAttachedActors(attachedActors);

	for (AActor* actor : attachedActors)
	{
		//ResetEffect if it already has it
		if (actor->GetClass() == StatusEffectClass)
		{
			Cast<AStatusEffect>(actor)->ResetEffectDuration();
			return nullptr;
		}
	}

	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;

	FVector Location = GetOwner()->GetActorLocation();

	if (AStatusEffect* statusEffect = GetWorld()->SpawnActor<AStatusEffect>(StatusEffectClass, Location, Rotation, SpawnInfo))
	{
		return statusEffect;
		//statusEffect->AttachInit(target, pcaster, power, duration, damageSpeed);
		//statusEffect->AttachToActor(target, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	}
	return nullptr;
}


// Called every frame
void UTowerAttackComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UTowerAttackComponent::setCanShootTrue()
{
	canShoot = true;
}

