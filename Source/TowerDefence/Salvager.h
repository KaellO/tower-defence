// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tower.h"
#include "Salvager.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENCE_API ASalvager : public ATower
{
	GENERATED_BODY()

public:
	ASalvager();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;
	void OnTargetEnter(AActor* target) override;
	void OnTargetExit(AActor* target) override;

	UPROPERTY(EditDefaultsOnly, Category = Stats)
	int32 extraGoldOnDeath;

	UPROPERTY(EditDefaultsOnly, Category = Stats)
	int32 goldAutoGen;

	UPROPERTY(EditDefaultsOnly, Category = Stats)
	float goldAutoGenTime;

	UPROPERTY(EditDefaultsOnly, Category = Stats)
	bool genGold;

	void GenerateGold();

private:
	FTimerHandle goldGenTimerHandle;
	void EnableGoldGen();
	class APlayerData* data;
};
