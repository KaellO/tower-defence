// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "CurrentLevelData.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENCE_API UCurrentLevelData : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 currentWave;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float waveProgress;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 totalEnemies;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 deadEnemies;

	void UpdateWaveProgress();
};
