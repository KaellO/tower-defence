// Fill out your copyright notice in the Description page of Project Settings.

#include "Salvager.h"
#include "TargetDetectionComponent.h"
#include "Enemy.h"
#include "PlayerData.h"
#include "Kismet/GameplayStatics.h"

ASalvager::ASalvager()
{
	genGold = true;
	goldAutoGenTime = 3;
}

void ASalvager::BeginPlay()
{
	Super::BeginPlay();
	data = Cast<APlayerData>(UGameplayStatics::GetActorOfClass(GetWorld(), APlayerData::StaticClass()));
	UTargetDetectionComponent* targetDetection = Cast<UTargetDetectionComponent>(GetComponentByClass(UTargetDetectionComponent::StaticClass()));
	if (targetDetection)
	{
		//targetDetection->OnTargetEnter.AddDynamic(this, &ASalvager::OnTargetEnter);
		//targetDetection->OnTargetExit.AddDynamic(this, &ASalvager::OnTargetExit);
	}
}

void ASalvager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	GenerateGold();
}

void ASalvager::OnTargetEnter(AActor* target)
{
	AEnemy* enemy = Cast<AEnemy>(target);
	enemy->goldReward += extraGoldOnDeath;
}

void ASalvager::OnTargetExit(AActor* target)
{
	AEnemy* enemy = Cast<AEnemy>(target);
	enemy->goldReward -= extraGoldOnDeath;
}

void ASalvager::GenerateGold()
{
	if (!genGold)
		return;

	genGold = false;
	if (data->IsValidLowLevel())
	{
		data->AddGold(goldAutoGen);
	}

	GetWorld()->GetTimerManager().SetTimer(goldGenTimerHandle, this, &ASalvager::EnableGoldGen, goldAutoGenTime, false);
}

void ASalvager::EnableGoldGen()
{
	genGold = true;
}
