// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "StatusEffect.h"
#include "BurningEffect.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENCE_API ABurningEffect : public AStatusEffect
{
	GENERATED_BODY()
public:
	virtual void Tick(float DeltaTime) override;
	ABurningEffect();
	void ApplyEffect() override;
};
