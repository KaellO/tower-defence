// Fill out your copyright notice in the Description page of Project Settings.

#include "LaserTower.h"
#include "BurningEffect.h"
#include "Enemy.h"

ALaserTower::ALaserTower()
{

}

AActor* ALaserTower::GetSingleTargetToAffect(TArray<AActor*> targets)
{
	if (targets.Num() <= 0)
		return nullptr;

	for (AActor* target : targets)
	{
		TArray<AActor*> attachedActors;
		target->GetAttachedActors(attachedActors);

		//If arrray is empty, return the target.
		if(attachedActors.Num() <= 0)
			return target;

		//Check if target has effect
		for (AActor* attachedActor : attachedActors)
		{
			//Break loop if if has effect.
			if (ABurningEffect* effect = Cast<ABurningEffect>(attachedActor))
			{
				break;
			}
			else
			{
				return target;
			}
		}
	}

	//If they all have, reapply the first one.
	return targets[0];
}
