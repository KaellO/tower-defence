// Fill out your copyright notice in the Description page of Project Settings.


#include "TowerGhost.h"
#include "TowerData.h"
#include "Tower.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/PlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "TowerNode.h"
#include "BuildManager.h"
#include "TargetDetectionComponent.h"
#include "Engine/StaticMesh.h"

// Sets default values
ATowerGhost::ATowerGhost()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	playerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
}

// Called when the game starts or when spawned
void ATowerGhost::BeginPlay()
{
	Super::BeginPlay();

	GetComponents<UStaticMeshComponent>(towerMeshComponents);

	buildManager = Cast<ABuildManager>(UGameplayStatics::GetActorOfClass(GetWorld(), ABuildManager::StaticClass()));

	if (!buildManager->IsValidLowLevel())
		return;

	if (buildManager->currentGhost->IsValidLowLevel())
	{
		Destroy();
	}
	else
	{
		buildManager->currentGhost = this;
	}

}

void ATowerGhost::ChangeGhostColor()
{
	if(towerMeshComponents.Num() <= 0)
		return;

	isBuildable = false;
	FHitResult hit;
	playerController->GetHitResultUnderCursorForObjects(ObjectTypes, true, hit);

	//GEngine->AddOnScreenDebugMessage(-1, 0, FColor::Cyan, hit.GetActor()->GetName());
	if (ATowerNode* node = Cast<ATowerNode>(hit.GetActor()))
	{
		if (node->currentTower == NULL)
		{
			for (UStaticMeshComponent* mesh : towerMeshComponents)
			{
				mesh->SetMaterial(0, ValidMaterial);
			}

			if (playerController->WasInputKeyJustReleased(FKey("LeftMouseButton")))
			{
				PlaceTower(node);
				buildManager->currentGhost = NULL;
				Destroy();
			}
			SetActorLocation(hit.GetActor()->GetActorLocation());
			isBuildable = true;
		}
	}
	else
	{
		for (UStaticMeshComponent* mesh : towerMeshComponents)
		{
			mesh->SetMaterial(0, InvalidMaterial);
		}
		FollowCursor();
	}
}

// Called every frame
void ATowerGhost::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	ChangeGhostColor();

	if (playerController->WasInputKeyJustReleased(FKey("LeftMouseButton")))
	{
		if (!isBuildable)
		{
			buildManager->currentGhost = NULL;
			Destroy();
		}
	}
	DrawCircle(GetWorld(), GetActorLocation(), FVector(1, 0, 0), FVector(0, 1, 0), FColor(0, 255, 0), TowerData->range * 3, 40, false, -1, 0, 40);
}

void ATowerGhost::FollowCursor()
{
	FVector WorldLocation;
	FVector WorldDirection;
	float DistanceAboveGround = 200.0f;

	playerController->DeprojectMousePositionToWorld(WorldLocation, WorldDirection);

	FVector PlaneOrigin(0.0f, 0.0f, DistanceAboveGround);

	FVector ActorWorldLocation = FMath::LinePlaneIntersection(
		WorldLocation,
		WorldLocation + WorldDirection,
		PlaneOrigin,
		FVector::UpVector);

	SetActorLocation(ActorWorldLocation);
}

void ATowerGhost::PlaceTower(ATowerNode* node)
{
	if (TowerData->IsValidLowLevel())
	{
		FRotator Rotation(0.0f, 0.0f, 0.0f);
		FActorSpawnParameters SpawnInfo;

		ATower* tower = GetWorld()->SpawnActor<ATower>(TowerData->towerClass, node->GetActorLocation(), Rotation, SpawnInfo);
		node->PlaceTower(tower);

		if (buildManager->IsValidLowLevel())
		{
			buildManager->DeductGold(TowerData->cost);
		}
	}
}



