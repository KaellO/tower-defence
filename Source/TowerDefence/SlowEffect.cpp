// Fill out your copyright notice in the Description page of Project Settings.

#include "SlowEffect.h"
#include "Enemy.h"
#include "GameFramework/FloatingPawnMovement.h"

ASlowEffect::ASlowEffect()
{

}

void ASlowEffect::ApplyEffect()
{
	AEnemy* enemy = Cast<AEnemy>(parentTarget);
	UFloatingPawnMovement* movement = Cast<UFloatingPawnMovement>(enemy->GetComponentByClass(UFloatingPawnMovement::StaticClass()));
	movement->MaxSpeed -= effectPower;
}

void ASlowEffect::EndEffect()
{
	AEnemy* enemy = Cast<AEnemy>(parentTarget);
	UFloatingPawnMovement* movement = Cast<UFloatingPawnMovement>(enemy->GetComponentByClass(UFloatingPawnMovement::StaticClass()));
	movement->MaxSpeed += effectPower;
	Destroy();
}
