// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TowerNode.generated.h"

UCLASS()
class TOWERDEFENCE_API ATowerNode : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATowerNode();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class ATower* currentTower;

	UFUNCTION()
	void PlaceTower(class ATower* tower);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UStaticMeshComponent* BaseMesh;

	UFUNCTION()
	void ClearNode();
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
