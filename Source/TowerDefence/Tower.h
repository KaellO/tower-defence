// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tower.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTowerSold);

USTRUCT(BlueprintType)
struct FTowerGraphics
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<class UStaticMesh*> staticMeshes;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<class UMaterial*> materials;
};

UCLASS()
class TOWERDEFENCE_API ATower : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATower();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UTowerData* TowerData;

	UPROPERTY(EditAnywhere, BlueprintAssignable)
	FTowerSold OnTowerSold;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 level;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 maxLevel;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UStaticMeshComponent* BaseMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UWidgetComponent* TowerMenu;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Graphics")
	TArray<FTowerGraphics> TowerGraphics;

	UFUNCTION(BlueprintCallable)
	void RotateTowards(class USceneComponent* rotatingPart, FVector targetLocation);

	UFUNCTION(BlueprintCallable)
	virtual TArray<AActor*> GetTargetsToAffect(TArray<AActor*> targets);

	UFUNCTION(BlueprintCallable)
	virtual AActor* GetSingleTargetToAffect(TArray<AActor*> targets);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float rotationSpeed;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TArray<UStaticMeshComponent*> staticMeshArray;

	UFUNCTION(BlueprintCallable)
	void Sell();

	UFUNCTION(BlueprintImplementableEvent)
	void OnUpgrade();

	void UpgradeMeshes();

	class APlayerData* playerData;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION(BlueprintCallable)
	virtual void OnTargetEnter(AActor* target);

	UFUNCTION(BlueprintCallable)
	virtual void OnTargetExit(AActor* target);

	UFUNCTION(BlueprintCallable)
	void ActivateMenu(UPrimitiveComponent* ClickedComp, FKey ButtonPressed);

	UFUNCTION(BlueprintCallable)
	void Upgrade();
};
