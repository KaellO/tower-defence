// Fill out your copyright notice in the Description page of Project Settings.

#include "Tower.h"
#include "Kismet/GameplayStatics.h"
#include "Components/StaticMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "TargetDetectionComponent.h"
#include "Components/WidgetComponent.h"
#include "PlayerData.h"
#include "TowerData.h"

// Sets default values
ATower::ATower()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));
	SetRootComponent(BaseMesh);

	TowerMenu = CreateDefaultSubobject<UWidgetComponent>(TEXT("TowerMenu"));
	TowerMenu->SetupAttachment(BaseMesh);
	TowerMenu->SetWidgetSpace(EWidgetSpace::Screen);

	level = 1;
	maxLevel = 3;
}

// Called when the game starts or when spawned
void ATower::BeginPlay()
{
	Super::BeginPlay();

	TowerMenu->RegisterComponent();
	TowerMenu->SetWorldLocation(GetActorLocation());
	TowerMenu->GetUserWidgetObject()->SetVisibility(ESlateVisibility::Hidden);

	playerData = Cast<APlayerData>(UGameplayStatics::GetActorOfClass(GetWorld(), APlayerData::StaticClass()));

	if (UTargetDetectionComponent* detection = Cast<UTargetDetectionComponent>(GetComponentByClass(UTargetDetectionComponent::StaticClass())))
	{
		if (TowerData->IsValidLowLevel())
		{
			if (TowerData->range > detection->DetectionRadius->GetScaledCapsuleHalfHeight())
			{
				detection->DetectionRadius->SetCapsuleHalfHeight(TowerData->range * 2);
			}
			detection->DetectionRadius->SetCapsuleRadius(TowerData->range);
		}
	}

	GetComponents<UStaticMeshComponent>(staticMeshArray);

	//Set Up Graphics and OnClick Event
	for (int i = 0; i < staticMeshArray.Num(); i++)
	{
		if (TowerGraphics.Num() == staticMeshArray.Num())
		{
			staticMeshArray[i]->SetStaticMesh(TowerGraphics[i].staticMeshes[0]);
			staticMeshArray[i]->SetMaterial(0, TowerGraphics[i].materials[0]);
		}

		staticMeshArray[i]->OnClicked.AddDynamic(this, &ATower::ActivateMenu);
	}
}

void ATower::RotateTowards(USceneComponent* rotatingPart, FVector targetLocation)
{
	FVector Direction = (GetActorLocation() - targetLocation);
	FRotator LookAtRot = FRotationMatrix::MakeFromX(Direction).Rotator();
	LookAtRot += FRotator(0, 90, 0);

	//FVector Direction = targetLocation - GetActorLocation();
	//FRotator LookAtRot = FRotationMatrix::MakeFromXZ(Direction, upVector).Rotator();
	//LookAtRot += FRotator(0, 90, 0);

	rotatingPart->SetRelativeRotation(FMath::RInterpTo(rotatingPart->GetComponentRotation(), LookAtRot, GetWorld()->DeltaTimeSeconds, rotationSpeed));
}

TArray<AActor*> ATower::GetTargetsToAffect(TArray<AActor*> targets)
{
	return TArray<AActor*>();
}

AActor* ATower::GetSingleTargetToAffect(TArray<AActor*> targets)
{
	if (targets.Num() > 0)
	{
		return targets[0];
	}
	return nullptr;
}

void ATower::Sell()
{
	OnTowerSold.Broadcast();
	playerData->AddGold(TowerData->cost / 2);
	Destroy();
}

void ATower::UpgradeMeshes()
{
	if (staticMeshArray.Num() <= 0)
		return;

	if (TowerGraphics[0].staticMeshes.Num() < level)
		return;

	for (int i = 0; i < staticMeshArray.Num(); i++)
	{
		if (TowerGraphics.Num() == staticMeshArray.Num())
		{
			staticMeshArray[i]->SetStaticMesh(TowerGraphics[i].staticMeshes[level - 1]);
			staticMeshArray[i]->SetMaterial(0, TowerGraphics[i].materials[level - 1]);
		}
	}
}

void ATower::Upgrade()
{
	if (level >= maxLevel)
		return;
	level++;
	playerData->AddGold(-TowerData->upgradeCost);
	UpgradeMeshes();
	OnUpgrade();
}

// Called every frame
void ATower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATower::OnTargetEnter(AActor* target)
{
}

void ATower::OnTargetExit(AActor* target)
{
}

void ATower::ActivateMenu(UPrimitiveComponent* ClickedComp, FKey ButtonPressed)
{
	if (TowerMenu->IsValidLowLevel())
	{
		//GEngine->AddOnScreenDebugMessage(-1, 3, FColor::Cyan, GetName());
		TowerMenu->GetUserWidgetObject()->SetVisibility(ESlateVisibility::Visible);
	}
}