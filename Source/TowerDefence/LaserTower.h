// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tower.h"
#include "LaserTower.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENCE_API ALaserTower : public ATower
{
	GENERATED_BODY()

public:
	ALaserTower();

protected:
	AActor* GetSingleTargetToAffect(TArray<AActor*> targets) override;
};
