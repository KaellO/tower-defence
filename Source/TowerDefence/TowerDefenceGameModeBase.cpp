// Copyright Epic Games, Inc. All Rights Reserved.


#include "TowerDefenceGameModeBase.h"
#include "EnemySpawner.h"
#include "Enemy.h"
#include "WaveData.h"
#include "Kismet/GameplayStatics.h"

float ATowerDefenceGameModeBase::UpdateWaveProgress()
{
	if (deadEnemies / totalEnemies == 1)
		WaveComplete();
	return deadEnemies/totalEnemies;
}

void ATowerDefenceGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AEnemySpawner::StaticClass(), FoundActors);

	if (FoundActors.Num() > 0)
	{
		for (int i = 0; i < FoundActors.Num(); i++)
		{
			if (AEnemySpawner* EnemySpawner = Cast<AEnemySpawner>(FoundActors[i]))
			{
				enemySpawners.Add(EnemySpawner);
				totalEnemies += EnemySpawner->currentWave->TotalEnemies();
				EnemySpawner->OnEnemySpawned.AddDynamic(this, &ATowerDefenceGameModeBase::OnEnemySpawned);
			}
		}
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 0.2f, FColor::Cyan, TEXT("No Spawners Found"));
	}
}

void ATowerDefenceGameModeBase::OnEnemySpawned(AEnemy* enemy)
{
	enemy->OnEnemyDeath.AddDynamic(this, &ATowerDefenceGameModeBase::OnEnemyDeath);
}

void ATowerDefenceGameModeBase::OnEnemyDeath()
{
	deadEnemies++;
}

void ATowerDefenceGameModeBase::WaveComplete()
{
	deadEnemies = 0;
	totalEnemies = 0;

	//GetWorld()->GetTimerManager().SetTimer(NextWaveHandle, this, &ATowerDefenceGameModeBase::SpawnNextWave, 3, false);
	canSpawnWave = true;
}

void ATowerDefenceGameModeBase::SpawnNextWave()
{
	canSpawnWave = false;
	for (AEnemySpawner* EnemySpawner : enemySpawners)
	{
		EnemySpawner->SpawnNextWave();
		totalEnemies += EnemySpawner->currentWave->TotalEnemies();
	}
}
