// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Enemy.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FEnemyDeath);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FKilledByPlayer, int32, reward);
UENUM()
enum EnemyType {
	FLYING UMETA(DisplayName = "FLYING"),
	GROUND UMETA(DisplayName = "GROUND"),
	BOSS UMETA(DisplayName = "BOSS")
};

UCLASS()
class TOWERDEFENCE_API AEnemy : public APawn
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnemy();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<AActor*> nodes;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float rotationSpeed;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UHealthComponent* HealthComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UFloatingPawnMovement* Movement;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UBoxComponent* Hitbox;


	UFUNCTION(BlueprintImplementableEvent)
	void OnDeath();
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void Die(AActor* killer);

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TEnumAsByte<EnemyType> enemyType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 goldReward;

	void FillNodes(TArray<AActor*> newNodes);

	UPROPERTY(EditAnywhere, BlueprintAssignable)
	FEnemyDeath OnEnemyDeath;

	UPROPERTY(EditAnywhere, BlueprintAssignable)
	FKilledByPlayer OnKilledByPlayer;

	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

private:
	void FollowNodes();

	int32 currentNodeIndex;
	float distance;
	class AAIController* AIController;

	//UENUM(BlueprintType)
	//	enum class Type : uint8 {
	//	FLYING UMETA(DisplayName = "FLYING"),
	//	GROUND UMETA(DisplayName = "GROUND")
	//}
};
