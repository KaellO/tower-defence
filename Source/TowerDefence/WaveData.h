// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "WaveData.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENCE_API UWaveData : public UDataAsset
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<TSubclassOf<class AEnemy>> EnemyWave;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FTimerHandle SpawnTimerHandle;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 WaveNumber;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float SpawnTimeInterval;

	UFUNCTION(BlueprintCallable)
	float TotalEnemies();
};
