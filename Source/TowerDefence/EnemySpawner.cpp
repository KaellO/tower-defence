// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemySpawner.h"
#include "WaveData.h"
#include "Math/UnrealMathUtility.h"
#include "HealthComponent.h"
#include "Enemy.h"

// Sets default values
AEnemySpawner::AEnemySpawner()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootSceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootSceneComponent"));
	SetRootComponent(RootSceneComponent);

	currentWaveIndex = 0;
}


// Called when the game starts or when spawned
void AEnemySpawner::BeginPlay()
{
	Super::BeginPlay();

	if (WaveDataArray.Num() > 0)
	{
		currentWave = DuplicateObject<UWaveData>(WaveDataArray[currentWaveIndex], this);

		if (currentWave->IsValidLowLevel())
			SpawnWave();
	}
}

void AEnemySpawner::SpawnNextWave()
{
	if (WaveDataArray.Num() > currentWaveIndex+1)
	{
		currentWaveIndex++;
		enemiesSpawned.Empty();

		currentWave = DuplicateObject<UWaveData>(WaveDataArray[currentWaveIndex], this);
		
		if (currentWave->IsValidLowLevel())
			SpawnWave();
	}
	else
	{
		allWavesSpawned = true;
	}
}

void AEnemySpawner::SpawnWave()
{
	if (currentWave->EnemyWave.Num() <= 0)
		return;

	indexOfEnemySpawned = 0;

	GetWorld()->GetTimerManager().SetTimer(currentWave->SpawnTimerHandle, this, &AEnemySpawner::SpawnEnemy, currentWave->SpawnTimeInterval, false);

}

// Called every frame
void AEnemySpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AEnemySpawner::SpawnEnemy()
{
	if (indexOfEnemySpawned >= currentWave->EnemyWave.Num())
		return;

	enemyToBeSpawned = currentWave->EnemyWave[indexOfEnemySpawned];

	if (enemyToBeSpawned->IsValidLowLevel())
	{
		TSubclassOf<class AEnemy> EnemyClass = enemyToBeSpawned;

		FRotator Rotation(0.0f, 0.0f, 0.0f);
		FActorSpawnParameters SpawnInfo;

		FVector Location = RootSceneComponent->GetComponentLocation();

		if (AEnemy* enemy = GetWorld()->SpawnActor<AEnemy>(EnemyClass, Location, Rotation, SpawnInfo))
		{
			if (secondNodes.Num() > 0)
			{
				int32 random = FMath::RandRange(0, 1);
				if (random == 0)
					enemy->FillNodes(nodes);
				else
					enemy->FillNodes(secondNodes);
			}
			else
			{
				enemy->FillNodes(nodes);
			}

			enemiesSpawned.Add(enemy);

			if (currentWaveIndex > 0)
			{
				enemy->goldReward += (currentWaveIndex * 2);
				enemy->HealthComponent->hp += (currentWaveIndex * 1.5);
			}
				

			OnEnemySpawned.Broadcast(enemy);

			indexOfEnemySpawned++;
		}

		if (indexOfEnemySpawned < currentWave->EnemyWave.Num())
		{
			GetWorld()->GetTimerManager().SetTimer(currentWave->SpawnTimerHandle, this, &AEnemySpawner::SpawnEnemy, currentWave->SpawnTimeInterval, false);
		}
		else
		{
			indexOfEnemySpawned = 0;
			currentWave = NULL;
			//Wave Completed 
		}
	}
}

