// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "StatusEffect.h"
#include "SlowEffect.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENCE_API ASlowEffect : public AStatusEffect
{
	GENERATED_BODY()

public:
	ASlowEffect();
	void ApplyEffect() override;
	void EndEffect() override;
};
