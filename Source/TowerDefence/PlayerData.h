// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PlayerData.generated.h"

UCLASS()
class TOWERDEFENCE_API APlayerData : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlayerData();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 Health;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 Gold;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void AddGold(int32 newGold);

	UFUNCTION(BlueprintImplementableEvent)
	void Dead();

private:
	TArray<class AMainCore*>Cores;

	class ABuildManager* buildManager;

	UFUNCTION()
	void CoreDamaged(int32 damage);

	UFUNCTION()
	void OnEnemySpawned(class AEnemy* enemy);

	UFUNCTION()
	void ReduceGold(int32 cost);
};
