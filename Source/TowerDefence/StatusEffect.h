// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "StatusEffect.generated.h"

UCLASS()
class TOWERDEFENCE_API AStatusEffect : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AStatusEffect();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	float effectDuration;
	float currentDuration;
	float effectPower;
	float effectSpeed;
	bool redoEffect;
	FTimerHandle effectTimerHandle;
	AActor* parentTarget;
	AActor* caster;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USceneComponent* root;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	virtual void ApplyEffect();

	UFUNCTION(BlueprintCallable)
	virtual void EndEffect();

	UFUNCTION(BlueprintCallable)
	virtual void AttachInit(AActor* target, AActor* pcaster, float power, float duration, float speed);

	UFUNCTION(BlueprintImplementableEvent)
	void StatusEffectVisuals();

	UFUNCTION(BlueprintCallable)
	bool BeginEffectOverTime();

	UFUNCTION(BlueprintCallable)
	void ResetEffectDuration();

	void EnableRedoEffect();
};
