// Fill out your copyright notice in the Description page of Project Settings.

#include "TargetDetectionComponent.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values for this component's properties
UTargetDetectionComponent::UTargetDetectionComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	DetectionRadius = CreateDefaultSubobject<UCapsuleComponent>("ObstacleArea");
	DetectionRadius->OnComponentBeginOverlap.AddDynamic(this, &UTargetDetectionComponent::OnCapsuleBeginOverlap);
	DetectionRadius->OnComponentEndOverlap.AddDynamic(this, &UTargetDetectionComponent::OnCapsuleEndOverlap);
	if(GetOwner()->IsValidLowLevel())
		DetectionRadius->SetupAttachment(GetOwner()->GetRootComponent());
}


// Called when the game starts
void UTargetDetectionComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


void UTargetDetectionComponent::OnCapsuleBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!targetClass->IsValidLowLevel())
		return;
	if (UKismetMathLibrary::ClassIsChildOf(OtherActor->GetClass(), targetClass))
	{
		if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
		{
			for (TEnumAsByte<EnemyType> type : enemyType)
			{
				if (type == enemy->enemyType)
				{
					GEngine->AddOnScreenDebugMessage(-1, 0.2f, FColor::Cyan, TEXT("Add"));
					targets.Add(OtherActor);
					OnTargetEnter.Broadcast(OtherActor);
					OnDetectionEnter();
				}
			}
		}
		else
		{
			GEngine->AddOnScreenDebugMessage(-1, 0.2f, FColor::Cyan, TEXT("Add"));
			targets.Add(OtherActor);
			OnTargetEnter.Broadcast(OtherActor);
			OnDetectionEnter();
		}
	}
}

void UTargetDetectionComponent::OnCapsuleEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex)
{
	if (targets.Num() > 0)
	{
		for (AActor* actor : targets)
		{
			if (actor == OtherActor)
			{
				OnTargetExit.Broadcast(OtherActor);
				OnDetectionExit();
				targets.Remove(OtherActor);
			}
		}
	}
}

// Called every frame
void UTargetDetectionComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UTargetDetectionComponent::DetectEnemies()
{
}

