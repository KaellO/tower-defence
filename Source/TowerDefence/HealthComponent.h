// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FZeroHealth, AActor*, killer);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDamageTaken);
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOWERDEFENCE_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
	void TakeDamage(int damage, AActor* damageDealer);

	UPROPERTY(EditAnywhere, BlueprintAssignable)
	FZeroHealth OnZeroHealth;

	UPROPERTY(EditAnywhere, BlueprintAssignable)
	FDamageTaken OnDamageTaken;

	UPROPERTY(EditAnywhere)
	int32 hp;
	UPROPERTY(EditAnywhere)
	int32 maxHp;
private:
		
};
