// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EnemySpawner.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FGetEnemySpawned, class AEnemy*, enemy);

UCLASS()
class TOWERDEFENCE_API AEnemySpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnemySpawner();

	UPROPERTY(EDITANYWHERE, BlueprintReadOnly)
	TArray<class UWaveData*> WaveDataArray;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UWaveData* currentWave;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 currentWaveIndex;

	UPROPERTY(EDITANYWHERE)
	TArray<AActor*> nodes;

	UPROPERTY(EDITANYWHERE)
	TArray<AActor*> secondNodes;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TArray<class AEnemy*> enemiesSpawned;

	UFUNCTION(BlueprintCallable)
	void SpawnNextWave();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	bool allWavesSpawned;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	bool levelComplete;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	void SpawnWave();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USceneComponent* RootSceneComponent;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintAssignable)
	FGetEnemySpawned OnEnemySpawned;

private:
	void SpawnEnemy();
	TSubclassOf<class AEnemy> enemyToBeSpawned;
	int32 indexOfEnemySpawned;
};
