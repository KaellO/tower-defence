 // Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerData.h"
#include "MainCore.h"
#include "Enemy.h"
#include "BuildManager.h"
#include "EnemySpawner.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
APlayerData::APlayerData()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Health = 100;
	Gold = 100;
}

// Called when the game starts or when spawned
void APlayerData::BeginPlay()
{
	Super::BeginPlay();
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AMainCore::StaticClass(), FoundActors);

	if (FoundActors.Num() > 0)
	{
		for (int i = 0; i < FoundActors.Num(); i++)
		{
			if (AMainCore* Core = Cast<AMainCore>(FoundActors[i]))
			{
				Cores.Add(Core);
			}
				
		}
		for (int i = 0; i < Cores.Num(); i++)
		{
			Cores[i]->OnCoreHit.AddDynamic(this, &APlayerData::CoreDamaged);
		}
	}
	FoundActors.Empty();

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AEnemySpawner::StaticClass(), FoundActors);

	if (FoundActors.Num() > 0)
	{
		for (int i = 0; i < FoundActors.Num(); i++)
		{
			if (AEnemySpawner* EnemySpawner = Cast<AEnemySpawner>(FoundActors[i]))
			{
				EnemySpawner->OnEnemySpawned.AddDynamic(this, &APlayerData::OnEnemySpawned);
			}
		}
	}

	buildManager = Cast<ABuildManager>(UGameplayStatics::GetActorOfClass(GetWorld(), ABuildManager::StaticClass()));
	buildManager->OnTowerBuilt.AddDynamic(this, &APlayerData::ReduceGold);
}

// Called every frame
void APlayerData::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void APlayerData::CoreDamaged(int32 damage)
{
	Health -= damage;

	if (Health <= 0)
	{
		Health = 0;
		Dead();
	}

	GEngine->AddOnScreenDebugMessage(-1, 0.35f, FColor::Cyan, TEXT("HIT"));
}

void APlayerData::OnEnemySpawned(AEnemy* enemy)
{
	enemy->OnKilledByPlayer.AddDynamic(this, &APlayerData::AddGold);
}

void APlayerData::ReduceGold(int32 cost)
{
	Gold -= cost;

	if (Gold < 0)
	{
		//This should never happen anyway.
		Gold = 0;
	}
}

void APlayerData::AddGold(int32 newGold)
{
	Gold += newGold;
}
