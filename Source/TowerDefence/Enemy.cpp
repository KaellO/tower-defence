// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy.h"
#include "AIController.h"
#include "Tower.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "Components/BoxComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/UnrealMathUtility.h"
#include "HealthComponent.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Hitbox = CreateDefaultSubobject<UBoxComponent>(TEXT("Hitbox"));
	SetRootComponent(Hitbox);

	Movement = CreateDefaultSubobject<UFloatingPawnMovement>(TEXT("Movement"));

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetupAttachment(Hitbox);


	rotationSpeed = 5;

	//SpawnDefaultController();
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();

	AIController = Cast<AAIController>(GetController());
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FollowNodes();
}

void AEnemy::FillNodes(TArray<AActor*> newNodes)
{
	for (int i = 0; i < newNodes.Num(); i++)
	{
		nodes.Add(newNodes[i]);
	}

	if (nodes.Num() > 0)
	{
		currentNodeIndex = 0;
	}
}

void AEnemy::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	TArray<AActor*> attachedActors;
	GetAttachedActors(attachedActors);

	for (AActor* actor : attachedActors)
		actor->Destroy();

	Super::EndPlay(EndPlayReason);
}

void AEnemy::FollowNodes()
{
	if (nodes.Num() <= 0)
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("No nodes"));
		return;
	}
	if (!AIController->IsValidLowLevel())
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("No default controller"));
		return;
	}
	
	distance = GetDistanceTo(nodes[currentNodeIndex]);
	if (distance < 200)
	{
		currentNodeIndex++;

		if (currentNodeIndex >= nodes.Num())
		{
			currentNodeIndex = 0;
		}
	}

	//FString TestHUDString = FString::SanitizeFloat(currentNodeIndex);
	//GEngine->AddOnScreenDebugMessage(-1, 2f, FColor::Cyan, TestHUDString);
	AIController->MoveToActor(nodes[currentNodeIndex], -1.0f, false, true);

	FVector Direction = (GetActorLocation() - nodes[currentNodeIndex]->GetActorLocation());
	FRotator LookAtRot = FRotationMatrix::MakeFromX(Direction).Rotator();
	LookAtRot = FRotator(0, LookAtRot.Yaw + 90, 0);
	
	SetActorRotation(FMath::RInterpTo(GetActorRotation(), LookAtRot, GetWorld()->DeltaTimeSeconds, rotationSpeed));
}
void AEnemy::Die(AActor* killer)
{
	if (Cast<ATower>(killer))
	{
		OnKilledByPlayer.Broadcast(goldReward);
	}
	OnEnemyDeath.Broadcast();
	OnDeath();
}

