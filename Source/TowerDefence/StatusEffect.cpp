// Fill out your copyright notice in the Description page of Project Settings.


#include "StatusEffect.h"
#include "Components/SceneComponent.h"

// Sets default values
AStatusEffect::AStatusEffect()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(root);

	redoEffect = true;
}

// Called when the game starts or when spawned
void AStatusEffect::BeginPlay()
{
	Super::BeginPlay();
	
}

void AStatusEffect::AttachInit(AActor* target, AActor* pcaster, float power, float duration, float speed)
{
	caster = pcaster;
	parentTarget = target;
	effectPower = power;
	effectDuration = duration;
	effectSpeed = speed;

	AttachToActor(target, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
}


bool AStatusEffect::BeginEffectOverTime()
{
	if (!redoEffect)
		return false;

	redoEffect = false;
	currentDuration++;
	ApplyEffect();
	GetWorld()->GetTimerManager().SetTimer(effectTimerHandle, this, &AStatusEffect::EnableRedoEffect, effectSpeed, false);

	if (currentDuration >= effectDuration)
	{
		Destroy();
		return false;
	}
	return true;
}

void AStatusEffect::EnableRedoEffect()
{
	redoEffect = true;
}

void AStatusEffect::ResetEffectDuration()
{
	currentDuration = 0;
}
// Called every frame
void AStatusEffect::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AStatusEffect::ApplyEffect()
{


}

void AStatusEffect::EndEffect()
{
}

